//console.log("Asdf");

// Array Methods
// JS has built-in functions and methods for arrays

// Mutator methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push method
// SYNTAX: arrayName.push()
//adds item(s) to end part

console.log("Current array:")
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruits.length);
console.log("Mutated array from push");
console.log(fruits);

// adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

// pop() method will remove end part of array

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// unshift()
// add element in the beginning of an array

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
// revores element in the beginning of an array

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice() method - simultaneously removes an element from specified index number and adds elements
// SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

fruits.splice(1, 2, "Lime", "Cherry")
console.log("Mutated array from splice method");
console.log(fruits);

// Sort - sort alphanumerically

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse - reverses order of array elements

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// non-mutator methods
// unlike the mutator, non-mutator methods do not modify array

let countries = ["US", "PH","CAN","SG","TH","PH","FR","DE"];

// indexof() - returns the index numbers
// if no element was found, returns -1 (falsy)
// SYNTAX -> arrayName.indexOf(searchValue, fromIndex (optional));

console.log(countries.indexOf("SG")); // 3
console.log(countries.indexOf("PH", 2)); //5

// lastIndexof() - returns index number of the last matching element in an array

console.log(countries.lastIndexOf("PH", 2)); //5

let lastIndex = countries.lastIndexOf("PH");
console.log("result of lastIndexOf method " + lastIndex);


let lastIndexStart = countries.lastIndexOf("PH, 4");
console.log("result of lastIndexOf method " + lastIndexStart);

// slice() - portions/slices elements from an array and returns a new array
// syntax -> arrayName.slice(startingIndex, endingIndex)

let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

// slicing off elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString() - returns an array as a string separated by commas
// SYNTAX -> arrayName.toString()

let stringArray = countries.toString();
console.log("Result from toString method");
console.log(stringArray);

// concat - combines two arrays and returns combined results
// syntax -> arrayA.concat(arrayB)

let taskArray1 = ["drink html","eat js"];
let taskArray2 = ["inhale css","breathe sass"];
let taskArray3 = ["get git","be node"];

let tasks = taskArray1.concat(taskArray2);
console.log("Result from concat method");
console.log(tasks);

// combining multiple arrays

console.log("Result from concat method");

let allTasks = taskArray1.concat(taskArray2, taskArray3)
console.log(allTasks);

// combining arrays with elements

console.log("Result from concat method");
let combinedTasks = taskArray1.concat("smell express", "throw react");
console.log(combinedTasks);

// join() - returns an array as a string separated by specified separator string
// syntax -> arrayName.join('separatorString')

let users = ["john", "jane", "joe", "robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' ~ '));

// iteration methods

// forEach
// similar to for loop that iterates on each array elements
// syntax -> arrayName.forEach(function(indivElement)){statemtn}

allTasks.forEach(function(task){
	console.log(task)
});

// using forEach with conditional statemnt

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length>10){
		filteredTasks.push(task);
	}
});

console.log("Result of filtered task");
console.log(filteredTasks);

// map() - this is useful for performing tasks where mutating or changing elements are required
// syneta -> let/cons resultArray = arrayName.map(function(indivElement))

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(num){
	return num * num;
});
console.log("Original Array");
console.log(numbers);
console.log("Result from map method");
console.log(numberMap);

// map vs forEach

let numberForEach = numbers.forEach(function(num){
	return num * num;
});
console.log(numberForEach); //undefined

// forEach(), loops over all items in the array as map(), but forEach() does not return new array




// every() - checks if all elements in an array meet the given condition
// syntax -> let/const resultArray = arrayName.every(finction(indivElement){condition})

let allValid = numbers.every(function(num){
	return (num < 3); //false because not all elements in array numbers is less than 3
});
console.log("Result from every method");
console.log(allValid);


// some() - checks if at least one in an array meet the given condition
// syntax -> let/const resultArray = arrayName.some(finction(indivElement){condition})

let allSomeValid = numbers.some(function(num){
	return (num < 3); //true because some elements in array numbers is less than 3
});
console.log("Result from some method");
console.log(allSomeValid);


// filter() - returns new array that contains elements that meet the given condition
// return an empty array if no found
// syntax -> let/const resultArray = arrayName.filter(function(indivElement){condition})

let filterValid = numbers.filter(function(num){
	return (num < 3);
});

console.log("Result from filter method");
console.log(filterValid);

// includes() - checks if the argument passed can be found in the array
// syntax -> arrayName.includes(<argumentToFind>)

let products = ["mouse", "keyboard","laptop","monitor"];

let productFound1 = products.includes("mouse");
console.log(productFound1); //true

let productFound2 = products.includes("mOuse");
console.log(productFound2); //false

// combined filter and inclide method

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})
console.log(filteredProducts);

// reduce() - evaluates elements from left to right and returns/reduces the array into a single value.
// syntax -> let/const resultArray = arrayName.reduce(function(accumulator, currentValue){operation})

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("current iteration:" + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	// the operation to reduce the array into single value

	return x + y;

});
console.log("Result of reduce method: " + reducedArray);

let list = ["hello", "again", "world"];
let reducedJoin = list.reduce(function(x, y){
	return x + " " + y;

});
console.log("Result of reduce method: " + reducedJoin);